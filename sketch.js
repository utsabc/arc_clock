function setup() {
  createCanvas(400,400);
  angleMode(DEGREES);
}

function draw() {
  translate(200,200);
  rotate(-90);
  background(100);
  strokeWeight(3);
  noFill();
  let h=hour();
  let m=minute();
  let s=second();
  
  stroke(245);
  let end=map(h%12,0,12,0,360);
  arc(0,0,200,200,0,end);
  
  stroke(240);
  let end1=map(m,0,60,0,360);
  arc(0,0,210,210,0,end1);
  
  stroke(235);
  let end2=map(s,0,60,0,360);
  arc(0,0,220,220,0,end2);
  
  strokeWeight(1);
  stroke(257);
  rotate(+90);
  text(h%12+':'+m+':'+s,-20,0)
}